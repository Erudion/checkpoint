# Implementation
This folder contains all scripts created for this project.

# Manual
A short summary of what this asset is and how to use it.

# UnityAsset
Contains an importable version of this project.


# Known limitations
*  A gameobject must have a unique name to be able to store its data using the checkpoint component
*  There are currently no icons attached to the project